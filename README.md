
Purpose: Read apache log files and publish backlinks as directory listing descriptions (via SetDescription directives in .htaccess files).

## Installation

	pip install apache-log-parser


## Links

* [apache-log-parser](https://github.com/rory/apache-log-parser): Python module to parse apache log files
* [Apache (2.4) Logs](https://httpd.apache.org/docs/2.4/logs.html)
* [Log format codes](https://httpd.apache.org/docs/2.4/mod/mod_log_config.html#formats)

## About

It's well known that the log files kept by a Apache contain information about what resources (pages, images, videos, etc) are accessed, and some information about who's accessing (IP address, browser type). In addition, Apache logs often contain the *REFERER* or URL of pages from which a resource is either linked or embedded. This gives crucial information about how the linking structure of your resources and allows the possibility to display links *back* to these referring sources (ie the *backlink*).

First, it's crucial that your log format includes the Referer information. Servers using the default Apache "[combined](https://httpd.apache.org/docs/2.4/logs.html#combined)" format include the Referer.

	LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" combined

See [Log format codes](https://httpd.apache.org/docs/2.4/mod/mod_log_config.html#formats) for more about the codes used.

Here a few lines from an actual log file:

	127.0.0.1 - - [27/Apr/2016:17:19:12 +0200] "GET /Electronic-textile-workshop/textielatelier.ogv HTTP/1.0" 206 38008 "http://video.constantvzw.org/Electronic-textile-workshop/textielatelier.ogv" "Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0"
	127.0.0.1 - - [27/Apr/2016:17:19:12 +0200] "GET /Electronic-textile-workshop/textielatelier.ogv HTTP/1.0" 206 511612024 "http://video.constantvzw.org/Electronic-textile-workshop/textielatelier.ogv" "Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0"
	127.0.0.1 - - [27/Apr/2016:17:22:31 +0200] "GET /Electronic-textile-workshop/textielatelier.ogv HTTP/1.0" 206 38008 "http://automatist.org/test/embed.html" "Mozilla/5.0 (X11; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0"

The first two lines are less interesting -- these are accesses to a movie from the movie itself. The third line however provides an HTTP_REFERER which comes from another server, namely an HTML page that embeds the given movie.


## Changelog

6 Oct 2017: Script rewrite to process a single file and output JSON.

