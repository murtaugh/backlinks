#!/usr/bin/env python
from __future__ import print_function
import apache_log_parser, sys
from urlparse import urlparse, urlunparse, urljoin
from argparse import ArgumentParser
from pprint import pprint
import urllib, json, gzip


COMBINED_LOG_FORMAT = "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\""

"""
# Map Apache Log file to a data structure...

{
     path: "video_access.log",
     last_time: "",
     referers_by_url: [
        {
            url: "",
            referers: [url, url...]    
        }
     ]
}

"""

def normalize_url (url, use_scheme="http"):
    scheme, netloc, path, params, query, fragment = urlparse(url)
    if use_scheme:
        scheme = use_scheme
    ret = urlunparse((scheme, netloc, path, params, query, ''))
    ret = ret.replace("%20", "+")
    return ret

class Logfile (object):
    """ Class to wrap an Apache Logfile for the purposes of creating a referers index (by request url) """
    def __init__ (self, path, logformat=COMBINED_LOG_FORMAT):
        if path.endswith(".gz"):
            self.path = path[-3:]
            self.gzip_path = path
        else:
            self.path = path
            self.gzip_path = None
        self.latest_ts = None
        self.referers_by_url = {}   # set objects keyed by request URL
        self.path_mappings = []     # [(path, baseurl), ...]
        self.exclude_referers = []          # (http://exclude.me/, http://ex.clu.de/me/too/, ...) ... criteria is url.startswith(x)
        self.logformat = logformat

    def addPathMapping (self, path, url):
        self.path_mappings.append((path, url))

    def excludeReferer(self, r):
        self.exclude_referers.append(r)

    def path_to_url (self, path):
        if type(path) == unicode:
            path = path.encode("utf-8")
        url = urllib.quote(path)
        # Apply path mappings
        for p, urlbase in self.path_mappings:
            if url.startswith(p):
                return urljoin(urlbase, url)
        return url

    def test_if_excluded (self, url):
        for x in self.exclude_referers:
            if url.startswith(x):
                return True

    def _record (self, url, referer):
        if (url not in self.referers_by_url):
            self.referers_by_url[url] = set()        
        referers = self.referers_by_url[url]
        if referer not in referers:
            referers.add(referer)

    def jsonify (self):
        ret = {}
        ret['path'] = self.path
        if self.latest_ts:
            ret['latest_ts'] = self.latest_ts
        ret['referers_by_url'] = [{"id": key, "referers": list(value)} for key, value in self.referers_by_url.items()]
        return ret

    def processLogFile (self):
        """ Process a logfile """
        line_parser = apache_log_parser.make_parser(self.logformat)
        if self.gzip_path:
            f = gzip.open(self.gzip_path)
        else:
            f = open(self.path)
        for line in f:
            row = line_parser(line)
            # pprint (row)
            referer = row['request_header_referer']
            # URL is relative (it's the path dammit)
            # referer is relative
            if referer and referer != "-":
                ts = row['time_received_utc_isoformat']
                if ts and (self.latest_ts == None or ts > self.latest_ts):
                    self.latest_ts = ts
                url = self.path_to_url(row['request_url_path'])
                # referer = normalize_url(referer)
                if self.test_if_excluded(referer):
                    continue
                self._record(url, referer)
        f.close()

    def dump (self, path):
        """ Dump state to JSON """
        pass

    def reload (self, path):
        """ Restore state from a previous JSON dump """
        pass

def process_logfile (x, mappings=None, logformat=COMBINED_LOG_FORMAT, exclude=None):
    lf = Logfile(x)
    if mappings:
        for p, url in mappings:
            lf.addPathMapping(p, url)
    if exclude:
        for x in exclude:
            lf.excludeReferer(x)
    lf.processLogFile()
    return lf.jsonify()

if __name__ == "__main__":

    p = ArgumentParser("")
    p.add_argument("input")
    p.add_argument("--logformat", default=COMBINED_LOG_FORMAT, help="format of apache log, default: combined")
    p.add_argument("--mappath", nargs=2, action="append", default=[], help="Add a path mapper, --path /some/path http://some.url/ .. can give multiply and are used in order until url matches (starts with(/some/path))")
    p.add_argument("--verbose", default=False, action="store_true")
    p.add_argument("--exclude", action="append", default=[], help="exclude REFERERs starting with this")
    args = p.parse_args()
    data = process_logfile(args.input, logformat=args.logformat, mappings=args.mappath, exclude=args.exclude)
    print (json.dumps(data, indent=2))